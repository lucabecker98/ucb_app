//
//  Dozent .swift
//  Stundenplan
//
//  Created by Vincent Dommer on 27.06.19.
//  Copyright © 2019 Vincent Dommer. All rights reserved.
//


class modulInfo{
    
    var dozent: String?
    var id: String?
    var name: String?
    var raum: String?
    
    init(dozent:String?, id: String?, name:String?, raum:String?){
        self.dozent = dozent;
        self.id = id;
        self.name = name;
        self.raum = raum;
    }
}
