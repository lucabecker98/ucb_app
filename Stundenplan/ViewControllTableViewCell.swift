//
//  ViewControllTableViewCell.swift
//  Stundenplan
//
//  Created by Vincent Dommer on 27.06.19.
//  Copyright © 2019 Vincent Dommer. All rights reserved.
//

import UIKit

class ViewControllTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mRaum: UILabel!
    @IBOutlet weak var mDozent: UILabel!
    @IBOutlet weak var mID: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
