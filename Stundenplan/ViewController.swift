//
//  ViewController.swift
//  Stundenplan
//
//  Created by Vincent Dommer on 13.06.19.
//  Copyright © 2019 Vincent Dommer. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var refArtists: DatabaseReference!
    
    @IBOutlet weak var tblDozent: UITableView!
    
    var artistList = [modulInfo]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllTableViewCell
        
        let artist: modulInfo
        
        artist = artistList[indexPath.row]
        
        cell.mName.text = artist.name
        cell.mRaum.text = artist.raum
        cell.mID.text = artist.id
        cell.mDozent.text = artist.dozent
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
        }
        
        refArtists = Database.database().reference().child("Module")
        
        refArtists.observe(DataEventType.value, with:{(snapshot) in
            if snapshot.childrenCount>0{
                self.artistList.removeAll()
            
                for Module in snapshot.children.allObjects as![DataSnapshot]{
                    let artistObject = Module.value as?[String: AnyObject]
                    let artistName = artistObject?["mName"]
                    let artistRaum = artistObject?["mRaum"]
                    let artistId = artistObject?["mID"]
                    let artistDozent = artistObject?["mDozent"]
                    
                    let artist = modulInfo(dozent: artistDozent as! String?, id: artistId as! String?, name: artistName as! String?, raum: artistRaum as! String?)
                    
                    self.artistList.append(artist)
                }
            }
            self.tblDozent.reloadData()
        })
    }


}

