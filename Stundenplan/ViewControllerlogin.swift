//
//  ViewControllerlogin.swift
//  Stundenplan
//
//  Created by Vincent Dommer on 28.06.19.
//  Copyright © 2019 Vincent Dommer. All rights reserved.
//

import UIKit
import Firebase

class ViewControllerlogin: UIViewController {

    @IBOutlet weak var benutzer: UITextField!
    @IBOutlet weak var passwort: UITextField!
    
    
    
    @IBAction func login(_ sender: Any) {
        guard
            let email = benutzer.text,
            let password = passwort.text,
           
            email.count > 0,
            password.count > 0
            else {
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Sign In Failed",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
            
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
